#include <stdio.h>

#define LOWER  0     /* lower limit of table */
#define UPPER  300   /* upper limit */
#define STEP   20    /* step size */

main() {
    printf("Hello, World!\n");

    /* print Fahrenheit-Celsius table
for fahr = 0, 20, ..., 300 */

    float fahr, celsius;
    float lower, upper, step;
    lower = 0;
    upper = 300;
    step = 20;
/* lower limit of temperature scale */
/* upper limit */
/* step size */

    fahr = lower;
    printf("Fahrenheit   Celsius\n");
    while (fahr <= upper) {
        celsius = (5.0 / 9.0) * (fahr - 32.0);
        printf("%6.0f\t\t %6.1f\n", fahr, celsius);
        fahr = fahr + step;
    }
    int fahr2;
    printf("---- BACKWARDS ----\n");
    printf("Fahrenheit   Celsius\n");
    for (fahr2 = UPPER; fahr2 >= LOWER; fahr2 = fahr2 - STEP)
        printf("%6d\t\t %6.1f\n", fahr2, (5.0 / 9.0) * (fahr2 - 32));
}
