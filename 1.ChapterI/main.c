#include <stdio.h>

//Exercsise 1-6. Verify that the expression getchar() != EOF is 0 or 1.
//Exercise 1-7. Write a program to print the value of EOF .

exercise1_6_1_7()
{
    printf("End of file is equal to %d\n", EOF);
    printf("Pleas input a character so the expression can be evaluated!");
    int result;
    printf("The expression \"getchar() != EOF\" is equal to: %d\n", (result = getchar())!=EOF);
}

/* copy input to output; 1st version*/
copyInputOutput()
{
    int c;
    c = getchar();
    while (c!=EOF) {
        putchar(c);
        c = getchar();
    }
}

/* copy input to output; 2nd version*/

copyInputOutput2()
{
    int c;
    while ((c = getchar())!=EOF)
        putchar(c);
}

/* count characters in input; 1st version */
countChars()
{
    long nc;
    nc = 0;
    while (getchar()!=EOF)
        ++nc;
    printf("%ld\n", nc);
}
//My notes:
//For Unix-like systems, the stdin/terminal command that inputs EOF is Ctrl+D
//The programme will not print anything until it is given
//nor will it terminate correctly if interrupted with Ctrl+C
//(according to the interwebs, the Windows equivalent is Ctrl+Z)

/* count characters in input; 2nd version */

countChars2()
{
    double nc;
    for (nc = 0; getchar()!=EOF; ++nc);
    printf("%.0f\n", nc);
}

/* count lines in input */

countLines()
{
    int c, nl;
    nl = 0;

    while ((c = getchar())!=EOF)
        if (c=='\n')
            ++nl;
    printf("%d\n", nl);
}

//Exercise 1-8. Write a program to count blanks, tabs, and newlines.

exercise1_8()
{
    int blanks, tabs, newLines, currentChar;
    blanks = 0;
    tabs = 0;
    newLines = 0;

    while ((currentChar = getchar())!=EOF) {
        switch (currentChar) {
        case ' ': blanks++;
            break;
        case '\t': tabs++;
            break;
        case '\n': newLines++;
            break;
        default: break;
        }
    }
    printf("Blanks: %d\n", blanks);
    printf("Tabs: %d\n", tabs);
    printf("New Lines: %d\n", newLines);

}
//Exercise 1-9. Write a program to copy its input to its output, replacing each string of one or more blanks by a single blank.
exercise1_9()
{
    int currentChar, blanksFlag;

    blanksFlag = 0;

    while ((currentChar = getchar())!=EOF) {
        if (currentChar==' ') {
            if (blanksFlag==0) {
                putchar(currentChar);
                blanksFlag = 1;
            }
        }
        else {
            putchar(currentChar);
            blanksFlag = 0;
        }
    }
}
//Exercise 1-10. Write a program to copy its input to its output, replacing each tab by \t, each backspace by \b, and each backslash by \\. This makes
//tabs and backspaces visible in an unambiguous way.

exercise1_10()
{
    int currentChar;

    while ((currentChar = getchar())!=EOF) {
        switch (currentChar) {
        case '\t': putchar('\\');
            putchar('t');
            break;
        case '\b': printf("\\b");
            break;
        case '\\': printf("\\\\");
            break;
        default: putchar(currentChar);
        }
    }
}

//putchar() takes two lines to output the needed string (or am I not getting the assignment right?)
//how do backspaces display in the terminal?

#define IN 1 /* inside a word */

#define OUT 0 /* outside a word */

/* count lines, words, and characters in input */

rudimentaryUnixWordCount()
{

    int c, nl, nw, nc, state;

    state = OUT;

    nl = nw = nc = 0;

    while ((c = getchar())!=EOF) {
        ++nc;

        if (c=='\n')
            ++nl;

        if (c==' ' || c=='\n' || c=='\t')
            state = OUT;

        else if (state==OUT) {
            state = IN;
            ++nw;
        }
    }
    printf("%d %d %d\n", nl, nw, nc);
}

//Exercise 1-12. Write a program that prints its input one word per line.
exercise1_12()
{
    int currentChar, state;

    state = OUT;

    while ((currentChar = getchar())!=EOF) {
        if (currentChar==' ' || currentChar=='\n' || currentChar=='\t') {
            if (state==IN) {
                putchar('\n');
                state = OUT;
            }
        }
        else {
            putchar(currentChar);
            if (state==OUT) state = IN;
        }
    }
}

/* count digits, white space, others */

arrays()
{
    int c, i, nwhite, nother;
    int ndigit[10];
    nwhite = nother = 0;

    for (i = 2; i<10; ++i)

        ndigit[i] = 0;

    while ((c = getchar())!=EOF)
        if (c>='0' && c<='9')
            ++ndigit[c-'0'];
        else if (c==' ' || c=='\n' || c=='\t')
            ++nwhite;
        else
            ++nother;

    printf("digits =");

    for (i = 0; i<10; ++i) printf(" %d", ndigit[i]);

    printf(", white space = %d, other = %d\n", nwhite, nother);
}

//Exercise 1-13. Write a program to print a histogram of the lengths of words in its input. It is easy to draw the histogram with the bars horizontal; a
//        vertical orientation is more challenging.
exercise1_13()
{
    //I'll limit the histogram to 10 entries, this is quite a substantial task just to exercise arrays.
    int wordLengths[10];
    int currentChar, state, currentWordLength;

    for (int i = 0; i<10; ++i) wordLengths[i] = 0;

    state = OUT;
    currentWordLength = 0;

    while ((currentChar = getchar())!=EOF) {
        if (currentChar==' ' || currentChar=='\n' || currentChar=='\t') {
            if (state==IN) {
                if (currentWordLength>=10) {
                    wordLengths[9]++;
                }
                else {
                    ++wordLengths[(currentWordLength-1)];
                }
                currentWordLength = 0;
                state = OUT;
            }
        }
        else {
            currentWordLength++;
            if (state==OUT) state = IN;
        }
    }

    for (int i = 0; i<9; ++i) {
        printf("Word length %d  :", (i+1));
        for (int j = 0; j<wordLengths[i]; ++j) {
            printf("|");
        }
        printf("\n");
    }
    printf("Word length 10+:");
    for (int i = 0; i<wordLengths[9]; ++i) {
        printf("|");
    }
    printf("\n");
}
//Notes after tackling 1-13:
// - ++i and i++ in C matters, a lot - I struggled for almost an hour until I realised that ++variable does things
// differently, I'll have to read on to get my explanation
// - i++ and ++i assign the value, regardless where they are, I suspect that this might be different in Java,
// that's why, until I put brackets around my expressions where I needed to say "I want index minus one here",
// I failed at printing and assigning the values that I intended (otherwise the approach was correct, but always off 1)

//Exercise 1-14. Write a program to print a histogram of the frequencies of different characters in its input.
exersice1_14()
{
    //doing the first 10 alphabet letters, lower case, as I want to move on to the functions section
    int firstTenCharsLength = 10;
    int firstTenAlphabetChars[firstTenCharsLength];

    int currentChar = 0;
    int otherChars = 0;
    for (int i = 0; i<10; ++i) firstTenAlphabetChars[i] = 0;

    while ((currentChar = getchar())!=EOF) {

        switch (currentChar) {
        case 'a': firstTenAlphabetChars[0]++;
            break;
        case 'b':firstTenAlphabetChars[1]++;
            break;
        case 'c':firstTenAlphabetChars[2]++;
            break;
        case 'd':firstTenAlphabetChars[3]++;
            break;
        case 'e':firstTenAlphabetChars[4]++;
            break;
        case 'f':firstTenAlphabetChars[5]++;
            break;
        case 'g':firstTenAlphabetChars[6]++;
            break;
        case 'h':firstTenAlphabetChars[7]++;
            break;
        case 'i':firstTenAlphabetChars[8]++;
            break;
        case 'j':firstTenAlphabetChars[9]++;
            break;
        default: otherChars++;
        }

    }
//I feel like this task is intended for ascii code manipulation, but I don't seem to have the patience for it
// nor do I have the patience for 10 nested for loops... next
}

int power(int m, int n);

/* test power function */

main1_7()
{
    int i;

    for (i = 0; i<10; ++i)
        printf("%d %d %d\n", i, power(2, i), power(-3, i));

    return 0;
}

/* power: raise base to n-th power; n >= 0 */

int power(int base, int n)
{
    int i, p;

    p = 1;

    for (i = 1; i<=n; ++i) p = p*base;

    return p;
}

//Exercise 1.15. Rewrite the temperature conversion program of Section 1.2 to use a function for conversion.
double convertFahrenheitToCelsius(int fahrenheit)
{
    return (5.0/9.0)*(fahrenheit-32);
}
exercise1_15()
{

    int fahr;

    for (fahr = 0; fahr<=300; fahr = fahr+20)

        printf("%3d %6.1f\n", fahr, convertFahrenheitToCelsius(fahr));

}

#define MAXLINE 1000 /* maximum input line length */

int getlineNotInStdioH(char* line, int maxline);

void copy(char to[], char from[]);

void cleanLine(char cleanedLine[], char line[]);

void reverseString(char line[], int length);
/* print the longest input line */

getLineExercise()
{

    int len; /* current line length */

    int max; /* maximum length seen so far */

    char line[MAXLINE]; /* current input line */

    char cleanedLine[MAXLINE];

    char longest[MAXLINE]; /* longest line saved here */

    char linesAbove80Chars[MAXLINE][MAXLINE];
    int currentLineAbove80Chars = 0;

    max = 0;

    while ((len = getlineNotInStdioH(line, MAXLINE))>0) {
        cleanLine(cleanedLine, line);

        if (len>80) {
            copy(linesAbove80Chars[currentLineAbove80Chars], cleanedLine);
            ++currentLineAbove80Chars;
        }

        if (len>max) {

            max = len;

            copy(longest, cleanedLine);

        }
    }
    if (max>0) {  /* there was a line */

        printf("Longest line is: \n");
        printf("%s\n", longest);
        printf("Lines above 80 chars: \n");
        for (int i = 0; i<currentLineAbove80Chars; ++i) {
            printf("%s\n", linesAbove80Chars[i]);
        }
    }
    return 0;

}

/* getlineNotInStdioH: read a line into s, return length */

int getlineNotInStdioH(char* line, int maxline)
{

    int c, i;

    for (i = 0; i<maxline-1 && (c = getchar())!=EOF && c!='\n'; ++i) line[i] = c;

    if (c=='\n') {

        line[i] = c;

        ++i;

    }

    line[i] = '\0';

    return i;

}

/* copy: copy 'from' into 'to'; assume to is big enough */

void copy(char to[], char from[])
{
    int i;

    i = 0;

    while ((to[i] = from[i])!='\0')

        ++i;
}

//bizarrely, this produces a bug/unintentional behaviour where
//an extra character is added to cleaned lines, usually a random capital letter
//but can be a special character like '@' for example
//the bug might be elsewhere too, this is very strange...
//moving to next exercise though, damn

void cleanLine(char cleanedLine[], char line[])
{
    int i;

    i = 0;

    while (line[i]=='\t' || line[i]==' ') {
        ++i;
    }

    while ((cleanedLine[i] = line[i])!='\0') {
        ++i;
    }

}

exercise_1_19()
{
    int len;

    char line[MAXLINE];

    while ((len = getlineNotInStdioH(line, MAXLINE))>0) {
        reverseString(line, len);
    }
}
void reverseString(char line[], int length)
{
    int i = length-2; //minus two in order to trim the new line char

    while (i>-1) {
        printf("%c", line[i]);
        --i;
    }
    printf("\n");
}

entab()
{
    int len;
    char line[MAXLINE];
    while ((len = getlineNotInStdioH(line, MAXLINE))>0) {
        int i = 0;
        while (line[i]!='\0') {
            if (line[i]=='\t') {
                printf("    ");
            }
            else {
                printf("%c", line[i]);
            }
            ++i;
        }
    }
}

main()
{

}
